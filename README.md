![](https://assets.emo.gg/logo/emoteville-gold3-small.png)
<br>
[Website](https://go.emo.gg/) - [Discord](https://go.emo.gg/discord) - [Issue Tracker](https://go.emo.gg/issue-tracker)

<br>
EmoteVilleSpawner is a spigot plugin that allows the player to interact with spawners without having to directly hook into any other plugin.

The original documantation for this project can be found [HERE](https://www.evernote.com/shard/s627/sh/6778d38a-8eaf-478c-8fcb-1b4821cd1d6f/3e8a39a297e38d13ded599e0bad11176).

It is possible to interact with other plugins using the commands and placeholders as an input/output system, this is how most of our plugins work.


**_Commands_**

| Command | Description |
| ------ | ------ |
| /spawner menuchange (type) (player) (coords) | Change the type of a spawner at certain coords. |
| /spawner change (type) | Change the spawner type you are looking at. |

**_Placeholders_**

| Placeholder | Description |
| ------ | ------ |
| %spawner_total% | Return the total amount of spawners that have been changed. |
| %spawner_total_(type)% | Return total amount of spawners for that type that have been changed. |
